import numpy as np
import math 
from scipy import integrate
from scipy.special import gamma 
import scipy.optimize as opt

def get_mue(ne,T):
	
	HBC = 197.3 
	mele = 0.511/HBC 
	theta = T/mele 
	
	# Get the initial guess for mue
	muu = ((3*np.pi**2*abs(ne))**(1.0/3.0) + mele)*1.1*HBC
	mul = 0.0
	
	if ne<0.0:
		mul = -muu
		muu = 0.0
	
	# Search for the actual solution
	def ff(x):
		return get_ne(x,T)/ne-1.0
	mue = opt.brentq(ff,mul,muu)
	
	return mue 

def get_ne(mue,T):
	
	HBC = 197.3 
	mele = 0.511 
	theta = T/mele 
	etae = (mue - mele)/T
	etab =-(mue + mele)/T 

	Fe1Half = FermiDirac(theta,etae,0.5) 
	Fb1Half = FermiDirac(theta,etab,0.5) 
	Fe3Half = FermiDirac(theta,etae,1.5) 
	Fb3Half = FermiDirac(theta,etab,1.5) 
		
	ne = (Fe1Half - Fb1Half) 	
	ne = ne + theta*(Fe3Half - Fb3Half) 	
	ne = math.sqrt(2)*mele**3/(np.pi**2*HBC**3)*math.pow(theta,1.5)*ne
	
	return ne

def FermiDirac(theta,eta,n):
	
	# Set up the integrand and integration intervals (choose based on eta)
	if eta<-35:
		def integrand(x):
			return math.pow(x,n)*math.sqrt(1.0 + 0.5*theta*x)*math.exp(-x)
		
		splits = [50/theta]
		splits.append(10*theta)	

	else:
		def integrand(x):
			a = x-eta 
			if (a>35.0):
				a = np.inf
			elif (a<-35.0):
				a = 0.0
			else:
				a = math.exp(x-eta)
			
			return math.pow(x,n)*math.sqrt(1.0 + 0.5*theta*x)/(a+1)
	
		# Setup integration intervals
		splits = [50/theta]
		splits.append(10*theta)
		if eta>5*theta: splits.append(eta-5*theta) 
		splits.append(eta+5*theta) 
		
	splits = sorted(splits) 		
	lowb = [0.0] 
	upb  = []
		
	for split in splits:
		if float(split)>0.0 and float(split)<np.inf:
			upb.append(float(split))
			lowb.append(float(split))
	upb.append(np.inf) 	
		
	# Integrate the intervals
	aa = 0.0
	aerr= 0.0
	epsrel = 1.e-12 
	for i in range(len(lowb)):
		(b, berr) = integrate.quad(integrand,lowb[i],upb[i],epsrel=epsrel)
		aa = aa + b
		aerr = aerr + berr
		
			
	if aerr/aa > 1.e-3 and eta>-5: 
		print 'Claimed integration precision is far from acceptable', aerr/aa, epsrel, theta, eta, n 	
	
	if eta<-35: aa = aa*math.exp(eta)

	return aa 

