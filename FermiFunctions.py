from scipy import integrate
from scipy.special import gamma 
import math 
import numpy as np 
import time 

def Beta(mass,Q,T,eta,matrix_element=4.6,Zdaughter=1,Adaughter=1):
	alpha = 1.0/137.0
	if (Q<mass):
		return 0.0	
	def integrand(x):
		# Calculate the Coulomb distortion as in FFN
		p = math.sqrt(x*x - mass/Q*mass/Q) 
		g = math.sqrt(1.0-(alpha*Zdaughter))
		y = alpha*Zdaughter*x/p 
		R = 1.25*pow(Adaughter,1/3) 
		Imag = 1j  
		G1 = abs(gamma(g + Imag*y))
		G2 = abs(gamma(2*g + 1.0))
		F = 2.0*(1.0+g)*math.pow(2.0*p*Q*R/197.3,-2.0*(1.0-g))*G1*G1/(G2*G2)*math.exp(math.pi*y) 
		
		# Calculate blocking factor
		a = x*Q/T - eta 
		if (a>228.0):
			a = 1.0 
		elif a<-228.0:
			a = 0.0
		else:
			a = math.exp(a)
			a = a/(a+1.0)

		return x*p*(x-1.0)*(x-1.0)*a*F
	 
	epsrel = 1.e-12 
	(b, berr) = integrate.quad(integrand,mass/Q,1.0,epsrel=epsrel,epsabs=0.0)
	if berr>1.e-6:
		print 'Bad beta',berr,Zdaughter,Adaughter
	rate = matrix_element*math.log(2)*(Q/mass)**5/6146*b

	return rate

def Capture(mass,Q,T,eta,matrix_element=4.6,Zparent=1,Aparent=1):
	alpha = 1.0/137.0	
	def integrand(x):
		# Calculate the Coulomb distortion as in FFN
		p = math.sqrt(x*x - mass/T*mass/T) 
		g = math.sqrt(1.0-(alpha*Zparent))
		y = alpha*Zparent*x/p 
		R = 1.25*pow(Aparent,1/3) 
		Imag = 1j  
		G1 = abs(gamma(g + Imag*y))
		G2 = abs(gamma(2*g + 1.0))
		F = 2*(1+g)*math.pow(2*p*T*R/197.3,-2*(1-g))*G1*G1/(G2*G2)*math.exp(math.pi*y) 
		
		# Calculate the blocking factor
		a = x-eta
		if (a>228.0):
			a = np.inf
		elif (a<-228.0):
			a = 0.0
		else:
			a = math.exp(a)

		return x*p*(x+Q/T)*(x+Q/T)/(a+1)*F
	 
	epsrel = 1.e-12
	low = max(-Q/T,mass/T)
	splits = [low] 
	if eta-5>low: splits.append(eta-5) 
	if eta+5>low: splits.append(eta+5) 
	splits = sorted(splits)
	splits.append(np.inf)
	a = 0.0
	aerr = 0.0
	for i in range(len(splits)-1):
		(b, berr) = integrate.quad(integrand,splits[i],splits[i+1],epsrel=epsrel)
		a = a + b 
		aerr = aerr + berr
	rate = matrix_element*math.log(2)*(T/mass)**5/6146*a

	return rate


