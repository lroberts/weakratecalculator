import numpy as np
import math 
import struct

class NuclearData:
"""
  Set of dictionaries for finding nuclear data from nuclear names, reads data from winv file
"""
	def __init__(self,winvFile):
		f = open(winvFile,'r')
		winv = f.readlines() 
		f.close() 

		i = 0
		self.name = {}
		self.mex  = {}
		self.A    = {}
		self.Z    = {}
		while i<len(winv):
			header = winv[i].split()
			i = i+4
			self.name[(int(header[2]),int(header[2])+int(header[3]))] = header[0]
			self.mex[header[0]] = float(header[-1])
			self.A[header[0]]   = int(header[2])+int(header[3]) 
			self.Z[header[0]]   = int(header[2]) 

class NuclearDecay:
"""
  Holds data about a particular decay, including the pre- and post-emission daughter
  nucleus
"""
	def __init__(self, parent, daughters, rate, nucData):
		self.rate_tot = rate
		self.parent = parent
		self.Zp = nucData.Z[parent]
		self.Ap = nucData.A[parent]
		self.Q00 = nucData.mex[parent] + 0.511
			
		self.Zds = [] 
		self.Ads = []
		self.daughters = []
		ds = daughters
		if not isinstance(ds,list): ds = [ds] 
		for daughter in ds:
			daughter = daughter.strip()
			if len(daughter)>0:
				self.Q00 = self.Q00 - nucData.mex[daughter] 
				self.daughters.append(daughter.strip())
				self.Zds.append(nucData.Z[daughter])
				self.Ads.append(nucData.A[daughter])
		self.Zd = sum(self.Zds)
		self.Ad = sum(self.Ads)
		self.daughter = nucData.name[(self.Zd,self.Ad)]
	
	def getName(self):
		print self.parent, " --> ", self.daughter, "(",
		for d in self.daughters:
			print d,
		print ")", self.rate_tot, self.Q00
	

def GetReaclibDecays(reaclibFile,winvFile):
"""
  Read a reaclib file and nuclear data file, extract the weak decay rates, and find 
  Q-values for the ground state to ground state transitions for these decays 
"""	
	# Read in the nuclear mass excesses
	nucData = NuclearData(winvFile)
	
	# Read in the vacuum decay rates 
	f = open(reaclibFile,'r')
	reaclib = f.readlines() 
	f.close() 
	
	# Set up the parser for the reaclib coefficients	
	Rates = []
	fieldwidths = (13,13,13,13) 
	fmtstring = ' '.join('{}{}'.format(abs(fw), 'x' if fw<0 else 's') for fw in fieldwidths) 
	fieldstruct = struct.Struct(fmtstring)
	parse = fieldstruct.unpack_from
	
	# iterate over file
	i = 0
	while i<len(reaclib):
		line = reaclib[i].split()
		try:
			chapter = int(line[0])
		except:
			i = i+1
			continue
		# Check if this is a chapter that contains decays
		if chapter==1 or chapter==2 or chapter==3 or chapter==11:
			i = i+1
			header = reaclib[i] 
			Parent   = ("".join(header[5:10].split()))
			Daughter = ("".join(header[10:15].split()))
			Daughter2 = ("".join(header[15:20].split()))
			Daughter3 = ("".join(header[20:25].split()))
			Daughter4 = ("".join(header[25:30].split()))
			Daughters = [Daughter, Daughter2, Daughter3, Daughter4]
			i = i+1
			data1  = np.array(parse(reaclib[i])).astype(np.float) 
			i = i+1
			data2  = np.array(parse(reaclib[i])[:-1]).astype(np.float)
			nonzero = np.sum(np.abs(data1[1:])) + np.sum(np.abs(data2))
			
			# Make sure this is a weak rate
			wflag = header[47]
			if nonzero>1.e-20 or wflag!="w": 
				continue 

			# Actually build the rate if we made it here 
			try:
				rat = NuclearDecay(Parent, Daughters, np.exp(data1[0]), nucData)
				if (rat.Q00>0.511): 
					if abs(rat.Zp-rat.Zd) > 1: 
						print "# This does not appear to be a beta decay: "
						print "#",
						rat.getName()
					else:
						Rates.append(rat)
				else:
					print "# Q value is too small: "
					print "#",
					rat.getName()
			except:
				print "# Could not make rate ", Parent, Daughters
			
		else:
			i = i+1
	return Rates

