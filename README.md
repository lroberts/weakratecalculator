# README #

Simple code for calculating beta-decay rates in medium assuming a single transition.  Vacuum rates are re-normalized to values tabulated elsewhere, namely REACLIB. 

### How do I get set up? ###

Just download and run.  Seems to work using the anaconda python distribution.

### Who do I talk to? ###

* Blame Luke for any and all problems (lroberts@tapir.caltech.edu)