#!/usr/bin/env python 
import electron 
from FermiFunctions import Beta, Capture
import numpy as np 
import math
from GetReaclibDecays import GetReaclibDecays
import h5py 
import optparse 

# Read arguments from command line
parser = optparse.OptionParser(description="Calculate weak rates for a set of ground state decay rates.") 
parser.add_option("-Q","--Qfactor",type="float",dest="Qfac",help="Q-value multiplier",default=1.0) 
parser.add_option("-M","--Qminimum",type="float",dest="Qmin",help="Minimum Q value",default=0.511) 
parser.add_option("-C","--Constant",action="store_false",dest="do_full",help="Assume vacuum rates for all points") 
(options, args) = parser.parse_args()
Qfac = options.Qfac
Qmin = options.Qmin
do_full = options.do_full 
if options.do_full is None: do_full = True

# Set up the grid
T9Grid = np.array([0.01, 0.1, 0.2, 0.4, 0.7, 1.0, 1.5, 2.0, 3.0, 5.0, 10.0, 30.0])
lrhoYeGrid = np.array([1.0,2.0,3.0,4,5,6,7,8,9,10,11])
beta = np.zeros((len(lrhoYeGrid),len(T9Grid))) 
ecap = np.zeros((len(lrhoYeGrid),len(T9Grid))) 
lnu   = -99.9*np.ones((len(lrhoYeGrid),len(T9Grid))) 
betai = np.zeros((len(lrhoYeGrid),len(T9Grid))) 
ecapi = np.zeros((len(lrhoYeGrid),len(T9Grid))) 
lnui  = -99.9*np.ones((len(lrhoYeGrid),len(T9Grid))) 

TMeVGrid = T9Grid*1.38066e-7/1.6021772e-6
neGrid = pow(10,lrhoYeGrid)*6.0221467e-16

mue_grid = np.zeros((len(TMeVGrid),len(neGrid)))
for idxT,T in enumerate(TMeVGrid):
	for idxne,ne in enumerate(neGrid):
		mue_grid[idxT,idxne] = electron.get_mue(ne,T) 

# Read in the reaclib ground state data
Rates = GetReaclibDecays("data/reaclib","data/winvn_v2.0.dat")

# Open the files
fname='bad_file.h5'
if (do_full): 
	fname = 'full_rates_Qfac' + str(options.Qfac) + "_Qmin" + str(options.Qmin) + ".h5" 
else:
	fname = 'vacuum_rates.h5'

f = h5py.File(fname,'w') 

# Iterate over reaclib rates
for rate in Rates:
	Q = max(rate.Q00*Qfac,Qmin)
	Zp = rate.Zp
	Ap = rate.Ap
	Zd = rate.Zd 
	Ad = rate.Ad 
	
	me = 0.511 
	
	if do_full:
		# First calculate the matrix element 
		eta = mue_grid[0,0]/TMeVGrid[0]
		if (Zp>Zd): 
			eta = -eta
		eta = 0.0
		T = 1.e-12
		beta_vac = max(Beta(me,Q,T,eta,matrix_element=1.0,Zdaughter=Zd,Adaughter=Ad),1.e-99)
		
		MM = rate.rate_tot/beta_vac
		beta_vac = beta_vac*MM
	
	print "# ", rate.parent, " --> ", 
	for d in rate.daughters:
		print d, 
	print " (and inverse)",
	if do_full: 
		print MM,Q
	else:
		print ""

	for idxT,T in enumerate(TMeVGrid):
		for idxne,ne in enumerate(neGrid):
			if do_full:
				eta = mue_grid[idxT,idxne]/T
				if (Zp>Zd): 
					eta = -eta
				
				beta[idxne,idxT] = math.log10(max(Beta(me,Q,T,eta,matrix_element=MM,
					Zdaughter=Zd,Adaughter=Ad),1.e-99))
				ecap[idxne,idxT] = math.log10(max(Capture(me,Q,T,-eta,matrix_element=MM,
					Zparent=Zp,Aparent=Ap),1.e-99))
				
				if 10.0**beta[idxne,idxT]>(1+1.e-8)*beta_vac: 
					print "The beta decay in medium should be smaller than the vacuum decay", 
					print beta_vac, 10.0**beta[idxne,idxT]

				betai[idxne,idxT] = math.log10(max(Beta(me,-Q,T,-eta,matrix_element=MM,
					Zdaughter=Zp,Adaughter=Ap),1.e-99))
				ecapi[idxne,idxT] = math.log10(max(Capture(me,-Q,T,eta,matrix_element=MM,
					Zparent=Zd,Aparent=Ad),1.e-99))
			else:	
				beta[idxne,idxT] = math.log10(rate.rate_tot) 
				ecap[idxne,idxT] = -99.9 
				
				betai[idxne,idxT] = -99.9
				ecapi[idxne,idxT] = -99.9
	
	
	idxT = 5
	idxne = 4

	print Ap,Zp,lrhoYeGrid[idxne],TMeVGrid[idxT],mue_grid[idxT,idxne], 
	print Q, 10.0**beta[idxne,idxT]/rate.rate_tot, 10.0**ecap[idxne,idxT]/rate.rate_tot, 
	print 10**betai[idxne,idxT]/rate.rate_tot, 10**ecapi[idxne,idxT]/rate.rate_tot 
	  
	grpNameForward = []  
	grpNameReverse = [] 
	
	dNames = ""
	for d in rate.daughters:
		dNames = dNames + "_" + d 
	 
	if (Zp>Zd):	
		grpNameForward = "betap_" + rate.parent + dNames
		grpNameReverse = "betam" + dNames + "_" + rate.parent
	else:
		grpNameForward = "betam_" + rate.parent + dNames 
		grpNameReverse = "betap" + dNames + "_" + rate.parent 
	
	try:	
		grp_for = f.create_group(grpNameForward) 
		grp_for.attrs.create("Description","Blah") 	
		grp_for.attrs.create("Source","REACLIB decay -> Matrix Element") 	
		grp_for.create_dataset("Parent",data=rate.parent) 
		grp_for.create_dataset("Daughters",data=rate.daughters) 
		grp_for.create_dataset("Parent_A",data=rate.Ap) 
		grp_for.create_dataset("Daughter_A",data=rate.Ads) 
		grp_for.create_dataset("Parent_Z",data=rate.Zp)
		grp_for.create_dataset("Daughter_Z",data=rate.Zds) 
		grp_for.create_dataset("T9grid",data=T9Grid)
		grp_for.create_dataset("logYeRhogrid",data=lrhoYeGrid)
		grp_for.create_dataset("beta",data=beta)
		grp_for.create_dataset("eps" ,data=ecap) 
		grp_for.create_dataset("lnu" ,data=lnu) 
	except:
		print "Could not create ", grpNameForward		
	try:	
		grp_rev = f.create_group(grpNameReverse) 
		grp_rev.attrs.create("Description","Blah") 	
		grp_rev.attrs.create("Source","REACLIB decay -> Matrix Element") 	
		grp_rev.create_dataset("Parent",data=rate.daughter) 
		grp_rev.create_dataset("Daughter",data=rate.parent) 
		grp_rev.create_dataset("Parent_A",data=rate.Ad) 
		grp_rev.create_dataset("Daughter_A",data=rate.Ap) 
		grp_rev.create_dataset("Parent_Z",data=rate.Zd) 
		grp_rev.create_dataset("Daughter_Z",data=rate.Zp) 
		grp_rev.create_dataset("T9grid",data=T9Grid)
		grp_rev.create_dataset("logYeRhogrid",data=lrhoYeGrid)
		grp_rev.create_dataset("beta",data=betai)
		grp_rev.create_dataset("eps" ,data=ecapi) 
		grp_rev.create_dataset("lnu" ,data=lnui) 
	except:
		print "Could not create ", grpNameReverse	
	
f.close()
